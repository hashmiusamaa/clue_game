'use strict';
// dynamically filling them when
// game starts using whatever images are
// found in html under class suspects and
// weapons
let all_array = {};
all_array['suspects'] = {};
all_array['rooms'] = {};
all_array['weapons'] = {};

// odd turns are user turns, even turns are computer turns.
let turn = 0;
let current_guess = {suspect:"", room:"", weapon:""};
let incident = {suspect:"", room:"", weapon:""};
let user_guesses = [];
let computer_guesses = [];

let undistributed_cards = {};
undistributed_cards['suspects'] = [];
undistributed_cards['rooms'] = [];
undistributed_cards['weapons'] = [];

let distributed_cards = {};
distributed_cards['user_cards'] = [];
distributed_cards['computer_cards'] = [];

let total_cards = 0;
let user_name;

let cards_to_guess_from = {weapons:[], rooms:[], suspects:[]};
let correctly_guessed = {weapon:"", room:"", suspect:""};

let known_wrongly_guessed = [];
known_wrongly_guessed['weapons'] = [];
known_wrongly_guessed['rooms'] = [];
known_wrongly_guessed['suspects'] = [];


let game_status = 0;
let count_wrong = 0;
let record;
let record_;
let turn_;
let history;
let history_;
let game;
let restart_;
let previous;