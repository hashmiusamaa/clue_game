'use strict';

function allow(ev) {
    if(game_status === 1){
        if(turn % 2 === 0){
            ev.preventDefault();
        }else{
            alert("Computer's TURN");
        }
    }
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    let data = ev.dataTransfer.getData("text");
    if(ev.target.classList.contains("room")){
        perform_drop(ev, data);
        switch_room(ev);
        if(document.getElementById(data).classList[0] === 'suspect'){
            current_guess.suspect = data;
        }else if(document.getElementById(data).classList[0] === 'weapon'){
            current_guess.weapon = data;
        }
    }
    if(current_guess.suspect !== ""
        && current_guess.weapon !== ""
        && current_guess.room !== ""){
        guess();
    }
}

function perform_drop(ev, data){
    let children_of_dropped = ev.target.childNodes;
    for(let img in children_of_dropped){
        if(children_of_dropped.hasOwnProperty(img)){
            if(children_of_dropped[img].tagName
                && children_of_dropped[img].tagName.toLowerCase() === "img"){
                // suspect already chosen for room
                if(children_of_dropped[img].id
                    && all_array['suspects'][children_of_dropped[img].id] !== undefined
                    && all_array['suspects'][data] !== undefined){
                    document.getElementById('suspects')
                        .appendChild(document.getElementById(children_of_dropped[img].id));
                }else
                // weapon already chosen for room
                if(children_of_dropped[img].id
                    && all_array['weapons'][children_of_dropped[img].id] !== undefined
                    && all_array['weapons'][data] !== undefined){
                    document.getElementById('weapons')
                        .appendChild(document.getElementById(children_of_dropped[img].id));
                }
            }
        }
    }
    ev.target.appendChild(document.getElementById(data));
}

function switch_room(ev){
    if(current_guess.room !== "" && current_guess.room !== ev.target.id){
        let prev_room = document.getElementById(current_guess.room).childNodes;
        // removing everything from previous room
        for(let node in prev_room){
            if(prev_room.hasOwnProperty(node)){
                p(prev_room[node]);
                // removing suspect from previous room
                if(prev_room[node].id
                    && all_array['suspects'][prev_room[node].id] !== undefined){
                    document.getElementById('suspects')
                        .appendChild(document.getElementById(prev_room[node].id));
                }else
                // removing weapons from previous room
                if(prev_room[node].id
                    && all_array['weapons'][prev_room[node].id] !== undefined){
                    document.getElementById('weapons')
                        .appendChild(document.getElementById(prev_room[node].id));
                }
            }
        }
        current_guess.weapon = "";
        current_guess.suspect = "";
        current_guess.room = ev.target.id;
    }else if(current_guess.room !== ev.target.id){
        current_guess.weapon = "";
        current_guess.suspect = "";
        current_guess.room = ev.target.id;
    }
}

function compare_with_incident(){
    let weapon_match = false;
    let room_match = false;
    let suspect_match = false;
    let revealed = false;
    record.innerHTML += " Murder was done by " +
        current_guess.suspect + " in " +
        current_guess.room + " using " +
        current_guess.weapon;
    previous.innerHTML = "Previous turn: Murder was done by " +
        current_guess.suspect + " in " +
        current_guess.room + " using " +
        current_guess.weapon + ". ";

    if(incident.weapon === current_guess.weapon){
        weapon_match = true;
    }else{
        previous.innerHTML += "Wrong weapon guess. ";
        record.innerHTML += ". Wrong weapon guess. ";
        known_wrongly_guessed['weapons'].push(current_guess.weapon);
        revealed = true;
    }
    if(incident.room === current_guess.room){
        room_match = true;
    }else{
        if(!revealed){
            previous.innerHTML += "Wrong room guess. ";
            record.innerHTML += ". Wrong room guess. ";
            known_wrongly_guessed['rooms'].push(current_guess.room);
            revealed = true;
        }
    }
    if(incident.suspect === current_guess.suspect){
        suspect_match = true;
    }else{
        if(!revealed) {
            previous.innerHTML += "Wrong suspect guess. ";
            record.innerHTML += ". Wrong suspect guess. ";
                known_wrongly_guessed['suspects'].push(current_guess.suspect);
        }
    }
    record.innerHTML += "<br>";
    return weapon_match && room_match && suspect_match;
}

function guess(){
    if(turn % 2 === 0){
        // player's turn
        user_guesses.push(current_guess);
        if(compare_with_incident()){
            record.innerHTML += "Congratulations! Your guess was correct. ";
            record.innerHTML += "Murder was done by " + current_guess.suspect + " in " +
                current_guess.room + " using " + current_guess.weapon;
            record.innerHTML += ". Game has been won by "+ user_name +". <br><br>";
            reset_guess();
            game_status = 0;
            // this part of the code to generate current date in UTC format is taken from
            // https://stackoverflow.com/questions/948532/how-do-you-convert-a-javascript-date-to-utc
            let now = new Date();
            let now_utc = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),  now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
            end_game(user_name + " vs Computer on " + now_utc +". Won by "+ user_name +" <br><br>");
        }else{
            setTimeout(()=>{
                    reset_guess();
                    record.innerHTML += "Computer's turn: ";
                    turn++;
                    show_computer_cards();
                    turn_.innerHTML = "Computer's turn";
                    execute_computer_turn()
                },
                2000);
        }
    }

}

function reset_guess(){
    document.getElementById('suspects').appendChild(document.getElementById(current_guess.suspect));
    document.getElementById('weapons').appendChild(document.getElementById(current_guess.weapon));
    current_guess = {suspect:"", room:"", weapon:""};
}