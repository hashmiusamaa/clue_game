'use strict';

function initialize_game(){
    let name = document.getElementById("name");
    user_name = document.getElementsByName("name")[0].value;
    if(user_name !== "" && user_name !== "computer" && user_name !== "Computer"){
        name.innerHTML = "Hello " + user_name + "! Welcome to clue!";
        record = document.getElementById("record");
        turn_ = document.getElementById("turn");
        record_ = document.getElementById("toggle_record");
        history_ = document.getElementById("toggle_history");
        restart_ = document.getElementById("restart");
        history = document.getElementById("history");
        game = document.getElementById("game");
        previous = document.getElementById("previous");
        show_element(record_.id);
        show_element(history_.id);
        end_game("");
        setup();
    }else{
        document.getElementsByName("name")[0].value = "";
    }
}

function re_init_game(){
    for(let a in all_array.suspects){
        if(all_array.suspects.hasOwnProperty(a)){
            show_element(all_array.suspects[a].id);
        }
    }
    for(let a in all_array.weapons){
        if(all_array.weapons.hasOwnProperty(a)){
            show_element(all_array.weapons[a].id);
        }
    }
    for(let a in all_array.rooms){
        if(all_array.rooms.hasOwnProperty(a)){
            show_element(all_array.rooms[a].id);
        }
    }
    all_array = {};
    all_array['suspects'] = {};
    all_array['rooms'] = {};
    all_array['weapons'] = {};
    turn = 0;
    current_guess = {suspect:"", room:"", weapon:""};
    incident = {suspect:"", room:"", weapon:""};
    user_guesses = [];
    computer_guesses = [];

    undistributed_cards['suspects'] = [];
    undistributed_cards['rooms'] = [];
    undistributed_cards['weapons'] = [];

    distributed_cards = {};
    distributed_cards['user_cards'] = [];
    distributed_cards['computer_cards'] = [];

    total_cards = 0;
    history.innerHTML = "";
    cards_to_guess_from = {weapons:[], rooms:[], suspects:[]};
    correctly_guessed = {weapon:"", room:"", suspect:""};
    known_wrongly_guessed['weapons'] = [];
    known_wrongly_guessed['rooms'] = [];
    known_wrongly_guessed['suspects'] = [];
    setup_suspects();
    setup_board("rooms");
    setup_weapons();
    incident_selection();
    start_game();
    hide_element(restart_.id);
    hide_element(record.id);
    game.removeAttribute("hidden");
}


function setup(){
    setup_suspects();
    setup_board("rooms");
    setup_weapons();
    incident_selection();
    start_game();
}

function generate_from_html(type_of_guess){
    let types = document.getElementById(type_of_guess);
    let images = types.getElementsByTagName("img");
    for(let type in images){
        if(images.hasOwnProperty(type)){
            let current_type =
                images[type];
            all_array[type_of_guess][current_type.id] = {id:current_type.id,
                img: current_type.src};
                current_type.removeAttribute("hidden");
                current_type.draggable = "true";
                current_type.alt = current_type.id;
        }
    }
    types.removeAttribute("hidden");
}

function setup_board(type_of_guess){
    let types = document.getElementById(type_of_guess);
    let spans = types.getElementsByTagName("span");
    for(let type in spans){
        if(spans.hasOwnProperty(type)){
            let current_type = spans[type];
            all_array[type_of_guess][current_type.id] = {id:current_type.id};
            current_type.removeAttribute("hidden");
            current_type.innerText = current_type.id;
            current_type.style.display = "inline-block";
            current_type.alt = current_type.id;
        }
    }
    types.removeAttribute("hidden");
}

function setup_suspects(){
    generate_from_html("suspects");
}

function setup_weapons(){
    generate_from_html("weapons");
}

function incident_selection(){
    undistributed_cards['suspects'] = JSON.parse(JSON.stringify(all_array['suspects']));
    undistributed_cards['rooms'] = JSON.parse(JSON.stringify(all_array['rooms']));
    undistributed_cards['weapons'] = JSON.parse(JSON.stringify(all_array['weapons']));
    incident['suspect'] = return_random_card_from('suspects');
    incident['room'] = return_random_card_from('rooms');
    incident['weapon'] = return_random_card_from('weapons');
}

function return_random_card_from(type){
    let len = Object.keys(undistributed_cards[type]).length;
    if(len > 0){
        let rand_type = Math.floor(Math.random()*len);
        let chosen_card = Object.keys(undistributed_cards[type])[rand_type];
        delete undistributed_cards[type][chosen_card];
        return chosen_card;
    }else{
        return undefined;
    }
}

function return_random_card(){
    let choose_from = Math.floor(Math.random()*3);
    let chosen_card;
    switch(choose_from){
        case 0:
        {
            chosen_card = return_random_card_from('suspects');
            choose_from = 'suspects';
            if(chosen_card !== undefined)
                break;
        }
        case 1:
        {
            chosen_card = return_random_card_from('rooms');
            choose_from = 'rooms';
            if(chosen_card !== undefined)
                break;
        }
        case 2:
        {
            chosen_card = return_random_card_from('weapons');
            choose_from = 'weapons';
            break;
        }
        default:{
            return return_random_card_from('suspects') || return_random_card_from('rooms');
        }
    }
    if(chosen_card !== undefined){
        return chosen_card
    }else{
        return undefined
    }
}

function distribute_cards(type){
    for(let i = 0; i < ((total_cards - 3)/2); i++){
        let random_card = return_random_card();
        if(random_card === undefined){
            i--;
        }else{
            distributed_cards[type+'_cards'].push(random_card);
        }
    }
}


function start_game(){
    total_cards = Object.keys(all_array['suspects']).length +
        Object.keys(all_array['rooms']).length +
        Object.keys(all_array['weapons']).length;
    distribute_cards('user');
    distribute_cards('computer');
    record.innerHTML = user_name+"'s turn: ";
    turn_.innerHTML = user_name+"'s turn";
    for(let users in distributed_cards.user_cards){
        for(let computers in distributed_cards.computer_cards)
        {
            //p(distributed_cards.user_cards[users] + " " + distributed_cards.computer_cards[computers]);
            if(distributed_cards.user_cards[users] === distributed_cards.computer_cards[computers]){
                //alert("FATAL ERROR! Game will reinitialize.");
                re_init_game();
            }
        }
    }
    show_user_cards();
    game_status = 1;
}


function show_user_cards(){
    hide_everything();
    distributed_cards.computer_cards.forEach((x) => {
        show_element(x);
    });
    remove_guesses();
}

function show_computer_cards(){
    hide_everything();
    distributed_cards.user_cards.forEach((x) => {
        show_element(x);
    });
    remove_guesses();
}

function hide_everything(){
    for(let suspect in all_array.suspects){
        if(all_array.suspects.hasOwnProperty(suspect)){
            hide_element(all_array.suspects[suspect].id);
        }
    }
    for(let weapon in all_array.weapons){
        if(all_array.weapons.hasOwnProperty(weapon)){
            hide_element(all_array.weapons[weapon].id);
        }
    }
    for(let room in all_array.rooms){
        if(all_array.rooms.hasOwnProperty(room)){
            hide_element(all_array.rooms[room].id);
        }
    }
    show_element(incident.weapon);
    show_element(incident.room);
    show_element(incident.suspect);
}


function show_element(x){
    document.getElementById(x).removeAttribute("hidden");
    document.getElementById(x).style.display = "inline-block";
}

function hide_element(x){
    document.getElementById(x).setAttribute("hidden","");
    document.getElementById(x).style.display = "none";
}

function remove_guesses(){
    for(let i in known_wrongly_guessed['weapons']){
        if(known_wrongly_guessed['weapons'].hasOwnProperty(i)){
            hide_element(known_wrongly_guessed['weapons'][i]);
        }
    }
    for(let i in known_wrongly_guessed['suspects']){
        if(known_wrongly_guessed['suspects'].hasOwnProperty(i)){
            hide_element(known_wrongly_guessed['suspects'][i]);
        }
    }
    for(let i in known_wrongly_guessed['rooms']){
        if(known_wrongly_guessed['rooms'].hasOwnProperty(i)){
            hide_element(known_wrongly_guessed['rooms'][i]);
        }
    }
    /*
        for(let i in computer_guesses){
            if(computer_guesses.hasOwnProperty(i)){
                if(computer_guesses[i].weapon !== incident.weapon){
                    hide_element(computer_guesses[i].weapon);
                }
                if(computer_guesses[i].room !== incident.room){
                    hide_element(computer_guesses[i].room);
                }
                if(computer_guesses[i].suspect !== incident.suspect){
                    hide_element(computer_guesses[i].suspect);
                }
            }
        }

        for(let i in user_guesses){
            if(user_guesses.hasOwnProperty(i)){
                if(user_guesses[i].weapon !== incident.weapon){
                    hide_element(user_guesses[i].weapon);
                }
                if(user_guesses[i].room !== incident.room){
                    hide_element(user_guesses[i].room);
                }
                if(user_guesses[i].suspect !== incident.suspect){
                    hide_element(user_guesses[i].suspect);
                }
            }
        }
    */
}

function toggle_record(){
    if(record.hasAttribute("hidden")){
        show_element(record.id);
        record_.value = "Hide history";
    }else{
        hide_element(record.id);
        record_.value = "Show history";
    }
}

function toggle_history(){
    if(history.hasAttribute("hidden")){
        show_element(history.id);
        game.setAttribute("hidden","");
        history_.value = "Hide record";
        let rec = get_record();
        let count_computer_won = (rec.match(/(Won by computer)/g) || []).length;
        let count_total_games = (rec.match(/(Won by)/g) || []).length;
        history.innerHTML = "Computer has won "+ count_computer_won +" games, and lost " +
            (count_total_games - count_computer_won).toString() +   " games. <br> Record: <br>";
        history.innerHTML += rec;
    }else{
        hide_element(history.id);
        game.removeAttribute("hidden");
        history_.value = "Show record";
    }
}
