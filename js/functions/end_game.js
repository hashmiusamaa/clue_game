'use strict';

function set_record(input){
    let data = localStorage.getItem("record");
    if(data === null) data = "";
    localStorage.setItem("record", data + input);
}

function get_record(){
    return localStorage.getItem("record");
}

function end_game(result){
    set_record(result);
    let rec = get_record();
    let count_computer_won = (rec.match(/(Won by computer)/g) || []).length;
    let count_total_games = (rec.match(/(Won by)/g) || []).length;
    history.innerHTML = "Computer has won "+ count_computer_won +" games, and lost " +
        (count_total_games - count_computer_won).toString() +   " games. <br> Record: <br>";
    history.innerHTML += rec;

    if(result!== ""){
        game.setAttribute("hidden","");
        history.disabled = true;
        show_element(restart_.id);
        show_element(record.id);
    }
}