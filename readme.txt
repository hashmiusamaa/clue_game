I have implemented the extra credit version of the game
fulfilling all the functional and non-functional requirements
but I have not done the drop down version of the game.

Since I have ensured that all the functional requirements are fulfilled,
I will be grateful if none or very few marks are cut as penalty.
The game is complete, even computer's turn shows properly.

In order to change the number of cards in the game,
add or remove elements in divs with ids "weapons",
"suspects" and "rooms".

For weapons and suspects, images need to be provided,
however for room, you may copy paste a current span
and just change id.

Thank you.

Extra credit 2:

phoenix.craiglist.org is adaptive but not responsive.
Changing the dimensions of the page does not change the layout.
However when refreshing the page when the browser window has smaller dimensions,
a completely new layout is loaded.

www.public.asu.edu/~kgary is a responsive page, changing size of the screen
is impacting the layout on the run in real time without
requiring a page load.

www.mlbtraderumors.com is adaptive but not responsive for the same reasons
phoenix.craiglist.org is adaptive and not responsive.